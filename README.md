# README #

Completed basic tutorial for endless runner. 

# TWEET/IDEAS FOR ADDITIONS #

Update endless runner with new skins/graphics.
Add Jumping section in which you must jump over a gap.
Add a Magnet item which pulls coins to you.
Add a sliding section?
Invincibility Item?
A "Back up life" which allows you to run into a rock once, but then keep on going instead of death